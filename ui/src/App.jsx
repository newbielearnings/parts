import React from "react";
import { MemoryRouter, Switch, Route } from "react-router-dom";
import Home from "./containers/Home";
// import FileObserver from "./containers/FileObserver";
import AppBar from "./containers/AppBar";
import Box from "@material-ui/core/Box";
import "toastr/build/toastr.css";

const Main = () => {
  return (
    <Box className="Main">
      <MemoryRouter initialEntries={["/file-observer"]} initialIndex={0}>
        <AppBar></AppBar>
        <Switch>
          <Route path={"/"} component={Home} />
        </Switch>
      </MemoryRouter>
    </Box>
  );
};

export default Main;
