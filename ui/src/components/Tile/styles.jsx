import { styled } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";

const Container = styled(Box)({
  width: 350,
  height: 200,
  boxShadow: "10px 10px 5px 0px rgba(0, 0, 0, 0.07)",
  position: "relative",
  margin: 10,
  textAlign: "center",
});

const TileUpperPart = styled(Box)({
  width: "100%",
  height: "50%",
  background: "black",
});

// const TileUpperPartText = styled(Box)({
//   color: "white",
//   paddingTop: 30,
// });

const TileUpperPartText = styled(Box)(({ color }) => ({
  color: color,
  paddingTop: 30,
}));

const TileMiddlePart = styled(Box)({
  width: 60,
  height: 60,
  marginTop: -30,
  marginLeft: "calc(50% - 30px)",
  zIndex: 100,
  borderRadius: "50%",
  background: "blue",
  position: "absolute",
});

const TileLowerPart = styled(Box)({
  width: "100%",
  height: "50%",
  background: "white",
});

const TileLowerPartText = styled(Box)({
  paddingTop: 50,
});

export {
  Container,
  TileUpperPart,
  TileUpperPartText,
  TileMiddlePart,
  TileLowerPart,
  TileLowerPartText,
  Box,
};
