import React from "react";

import {
  Container,
  TileUpperPart,
  TileUpperPartText,
  TileMiddlePart,
  TileLowerPart,
  TileLowerPartText,
} from "./styles";

const Tile = ({ upperText, lowerText }) => (
  <Container>
    <TileUpperPart>
      <TileUpperPartText color="white">{upperText}</TileUpperPartText>
    </TileUpperPart>
    <TileMiddlePart></TileMiddlePart>
    <TileLowerPart>
      <TileLowerPartText>{lowerText}</TileLowerPartText>
    </TileLowerPart>
  </Container>
);

export default Tile;
