import { createMuiTheme } from "@material-ui/core/styles";

const getMuiTheme = () =>
  createMuiTheme({
    palette: {
      primary: {
        main: "rgba(0,0,0,0.87)",
      },
      // secondary: {
      //   light: "rgba(0,0,0,0.17)",
      //   main: "rgba(0,0,0,0.17)",
      // },
    },
    overrides: {
      //   MUIDataTableHeadRow: {
      //     root: {
      //       backgroundColor: "#0a0a46",
      //       color: "#fff"
      //     },
      //   },
      MUIDataTableHeadCell: {
        root: {
          backgroundColor: "#fff",
          color: "#000",
        },
        fixedHeader: {
          backgroundColor: "#eadadade",
          color: "#000",
        },
        sortActive: {
          color: "#fff",
          backgroundColor: "#027784",
        },
      },
      MUIDataTableToolbar: {
        root: { display: "none" },
      },
      MUIDataTableSelectCell: {
        headerCell: {
          backgroundColor: "#eadadade",
        },
      },
      MUIDataTableHead: {
        main: {
          borderTop: "5px solid #dada14",
          borderBottom: "1px solid #000",
        },
      },
      MUIDataTableBodyRow: {
        root: {
          borderTop: "1px solid #000",
          background: "#fff",
          "&:hover": {
            background: "#fff !important",
            color: "#000",
          },
        },
        // hover: {
        //   background: "red",
        //   color: "red",
        // },
      },
      MUIDataTableFooter: {
        root: {
          display: "none",
        },
      },
      //   MUIDataTable: {
      //     paper: {
      //       padding: "0 10px",
      //     },
      //   },
      MuiButton: {
        label: {
          textTransform: "none",
        },
      },
      MuiFormControl: {
        root: {
          width: "100%",
        },
      },
      MuiFilledInput: {
        inputMarginDense: {
          paddingTop: 15,
          paddingBottom: 15,
        },
      },
    },
  });

export default getMuiTheme;
