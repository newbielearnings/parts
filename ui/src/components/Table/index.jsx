import React, { useState, useEffect, useRef } from "react";
import { makeStyles } from "@material-ui/core/styles";
import DTable, { ExpandButton } from "mui-datatables";
import fileAPIs from "../../apis/FileObserver/file";
import Pagination from "./Pagination";
import { MuiThemeProvider } from "@material-ui/core/styles";
import getMuiTheme from "./theme";

import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import Box from "@material-ui/core/Box";
import SearchBar from "./SearchBar";
import Typography from "@material-ui/core/Typography";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
  faArrowAltCircleRight,
  faSortUp,
  faSortDown,
} from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#fff",
    height: "100%",
    padding: "0 10px",
  },
  tableContainer: {
    // marginTop: "20vh",
    paddingTop: 10,
    "& thead th:nth-child(2)": {
      width: "50%",
    },
    "& thead th:nth-child(3)": {
      width: "30%",
    },
  },

  tableHeadCell: {
    background: "#eadede",
    color: "#000",
  },

  iconContainer: {
    marginLeft: 3,
  },
}));

const ColumnWithSortIcon = (
  classes,
  columnMeta,
  handleToggleColumn,
  sortOrder
) => {
  let icon;
  if (columnMeta.name === sortOrder.name) {
    icon = sortOrder.direction === "desc" ? faSortUp : faSortDown;
  }

  return (
    <TableCell
      className={classes.tableHeadCell}
      key={columnMeta.index}
      onClick={() => handleToggleColumn(columnMeta.index)}
      style={{ cursor: "pointer" }}
    >
      {columnMeta.name}
      <FontAwesomeIcon className={classes.iconContainer} icon={icon} />
    </TableCell>
  );
};

const ColumnWithEndIcon = (
  classes,
  columnMeta,
  handleToggleColumn,
  sortOrder
) => {
  return (
    <TableCell
      className={classes.tableHeadCell}
      key={columnMeta.index}
      style={{ cursor: "pointer" }}
    >
      <FontAwesomeIcon
        className={classes.iconContainer}
        icon={faArrowAltCircleRight}
      />
    </TableCell>
  );
};

const getTableOptions = ({ paginationRef, classes }) => ({
  // filter: true,
  // filterType: "dropdown",
  responsive: "standard",
  expandableRows: false,
  expandableRowsHeader: false,
  expandableRowsOnClick: true,
  elevation: 0,
  export: false,
  print: false,
  search: true,
  download: false,
  filter: true,
  viewColumns: true,
  // sort: true,
  selectableRows: "none",
  onFilterChange: () => {
    console.log("cha cha", paginationRef);
    paginationRef.current && paginationRef.current.refreshPagination();
  },
  // customToolbar: () => <div />,
  // customToolbarSelect: () => <div />,
  // searchOpen: true,
  //   isRowExpandable: (dataIndex, expandedRows) => {
  //     if (dataIndex === 3 || dataIndex === 4) return false;

  //     // Prevent expand/collapse of any row if there are 4 rows expanded already (but allow those already expanded to be collapsed)
  //     if (
  //       expandedRows.data.length > 4 &&
  //       expandedRows.data.filter((d) => d.dataIndex === dataIndex).length === 0
  //     )
  //       return false;
  //     return true;
  //   },
  // rowsExpanded: [0, 1],
  // renderExpandableRow: (rowData, rowMeta) => {
  //   return [
  //     <ExpandTableHead className="expand-content">
  //       <ExpandTableCell colSpan={1}></ExpandTableCell>
  //       {rowData.map((d) => (
  //         <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //       ))}
  //     </ExpandTableHead>,
  //     [1, 2, 3].map(() => {
  //       return (
  //         <ExpandTableRow className="expand-content">
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           <ExpandTableCell colSpan={1}></ExpandTableCell>
  //           {rowData.map((d) => (
  //             <ExpandTableCell colSpan={1}>{d}</ExpandTableCell>
  //           ))}
  //         </ExpandTableRow>
  //       );
  //     }),
  //   ];
  // },
  //   renderExpandableRow: (rowData, rowMeta, a) => {
  //     return (
  //       <TableRow>
  //         <TableCell colSpan={1} />
  //         <TableCell colSpan={rowData.length - 1}>
  //           <Table
  //             className="expandFileDataTable"
  //             title={"Employee list"}
  //             data={[
  //               {
  //                 id: "ABC123",
  //                 date: "2020-06-24T06:36:46.728Z",
  //                 type: "Fire",
  //                 description: "Hot",
  //                 instructions: "",
  //               },
  //               {
  //                 id: "DEF456",
  //                 date: "2020-06-24T06:36:46.728Z",
  //                 type: "Water",
  //                 description: "Wet",
  //                 instructions: "",
  //               },
  //             ]}
  //             columns={[
  //               {
  //                 name: "id",
  //                 label: "ID",
  //               },
  //               {
  //                 name: "type",
  //                 label: "Type",
  //               },
  //               {
  //                 name: "description",
  //                 label: "Description",
  //               },
  //               {
  //                 name: "date",
  //                 label: "Date",
  //               },
  //             ]}
  //             options={{
  //               download: false,
  //               filter: false,
  //               pagination: false,
  //               print: false,
  //               search: false,
  //               viewColumns: false,
  //               expandableRowsHeader: false,
  //               elevation: 0,
  //               selectableRows: "none",
  //               selectToolbarPlacement: "none",
  //               fixedSelectColumn: false,
  //             }}
  //             components={components}
  //           />
  //         </TableCell>
  //       </TableRow>
  //     );
  //   },
  onRowExpansionChange: (curExpanded, allExpanded, rowsExpanded) =>
    console.log(curExpanded, allExpanded, rowsExpanded),
});

const components = {
  ExpandButton: function (props) {
    if (props.dataIndex === 3 || props.dataIndex === 4)
      return <div style={{ width: "24px" }} />;
    return <ExpandButton {...props} />;
  },
};

const parseColumns = (columns, classes) => {
  const temp = columns.map((c) => {
    const options = {
      sortThirdClickReset: true,
      customHeadRender: ColumnWithSortIcon.bind(this, classes),
      ...c.options,
    };

    return {
      name: c.name,
      options,
    };
  });
  temp.push({
    name: "",
    options: {
      customBodyRender: ColumnWithEndIcon.bind(this, classes),
    },
  });
  return temp;
};

const filterData = (data, key) => {
  if (!key) return data;
  return data.filter((d) => {
    if (d.join().toLowerCase().indexOf(key.toLowerCase()) !== -1) {
      return true;
    }
  });
};

const Table = (props) => {
  const tableRef = useRef(null);
  const paginationRef = useRef(null);
  const classes = useStyles();

  const columns = parseColumns(props.columns, classes);
  const [filter, setFilter] = useState("");

  const fetchTableData = () => {
    const tableData = (props.data || []).map((row) => [...row, "icon-right"]);
    return filterData(tableData, filter);
  };

  const [filteredTableData, setFilteredTableData] = useState(fetchTableData());

  const refresh = () => {
    console.log("refresh");
    setFilteredTableData(fetchTableData());
    paginationRef.current && paginationRef.current.refreshPagination();
  };

  useEffect(() => {
    refresh();
  }, [props.data, filter]);

  useEffect(() => {
    refresh();
  }, []);

  const fetchFileDataFromServer = async () => {
    // setIsFileDataLoading(true);
    // const fileData = await fileAPIs.fetchData();
    // dispatch(updateFileData(fileData));
    // setTimeout(() => {
    //   setIsFileDataLoading(false);
    // }, 600);
  };

  const handleFilterChange = (text) => {
    setFilter(text);
  };

  return (
    <MuiThemeProvider theme={getMuiTheme()}>
      <Box className={classes.root}>
        <Box className={classes.tableContainer}>
          <Typography variant="h5">{props.title}</Typography>
          <SearchBar onChange={handleFilterChange} />
          <DTable
            className="fileDataTable"
            ref={tableRef}
            title={props.title}
            data={filteredTableData}
            columns={columns}
            options={getTableOptions({ paginationRef, classes })}
            components={components}
          />
        </Box>
        <Pagination tableRef={tableRef} ref={paginationRef} />
      </Box>
    </MuiThemeProvider>
  );
};

export default Table;
