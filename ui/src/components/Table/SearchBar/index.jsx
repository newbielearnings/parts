import React, {
  Component,
  useState,
  useRef,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from "react";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Select from "../../Select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { makeStyles } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Input from "@material-ui/core/Input";
import FilledInput from "@material-ui/core/FilledInput";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

import { faSearch } from "@fortawesome/free-solid-svg-icons";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    margin: "10px 0",
  },
  formControl: {
    width: "25%",
  },
  icon: {
    opacity: "60%",
  },
  label: {
    padding: "0 5px;",
  },
  dense: {
    paddingTop: 6,
  },
  button: {
    width: 100,
    background: "#fff !important",
    border: "1px solid rgba(0,0,0,0.2)",
    outline: "none",
    background: "#000 !important",
    color: "#fff",
    height: 50,
    marginLeft: 10,
  },
}));

const SearchBar = (props) => {
  useEffect(() => {}, []);

  const [text, setText] = React.useState("");

  const handleChangeText = (e) => {
    setText(e.target.value);
  };

  const handleSubmit = (e) => {
    props.onChange(text);
  };

  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <FormControl className={classes.formControl}>
        <FilledInput
          margin="dense"
          className={classes.input}
          id="search-adornment"
          value={text}
          onChange={handleChangeText}
          placeholder="Search"
          endAdornment={
            <InputAdornment position="end">
              <FontAwesomeIcon
                className={classes.icon}
                icon={faSearch}
              ></FontAwesomeIcon>
            </InputAdornment>
          }
        />
      </FormControl>
      <Button className={classes.button} onClick={handleSubmit}>
        Search
      </Button>
    </Box>
  );
};

export default SearchBar;
