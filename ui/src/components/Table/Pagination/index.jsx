import React, {
  Component,
  useState,
  useRef,
  useEffect,
  useImperativeHandle,
  forwardRef,
} from "react";

import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Select from "../../Select";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { faArrowRight, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    marginTop: 10,
    zIndex: 1000,
    marginRight: 100,
    "&> *": {
      width: 100,
      margin: "auto 10px",
      maxHeight: 40,
      height: 40,
      lineHeight: "40px",
      userSelect: "none",
    },
  },
  pageSelect: {
    lineHeight: "20px",
    paddingTop: 12,
    borderRadius: 3,
    width: 75,
    border: "1px solid rgba(0,0,0,0.2)",
  },
  textLabel: {
    width: "fit-content",
  },
  actionsButtons: {
    width: 50,
    background: "#fff !important",
    color: "rgba(0, 0, 0, 0.87)",
    outline: "none",
  },
  paginationBlock: {
    width: "fit-content",
    padding: "0px 5px",
    color: "rgba(0,0,0,0.87)",
    fontWeight: 500,
    textDecoration: "underline green solid",
    borderRadius: 3,
    textUnderlineOffset: "5px",
    outline: "none",
  },
  conContainer: {
    marginLeft: 3,
  },
}));

const fetchTableAttributes = (tableRef) => {
  if (!tableRef || !tableRef.current) return {};
  const ref = tableRef.current;
  console.log("fetchTableAttributes - ", ref.state);
  return {
    currentPage: ref.state.page,
    totalPages:
      1 +
      Math.floor((ref.state.displayData.length - 1) / ref.state.rowsPerPage),
    rowsPerPage: ref.state.rowsPerPage,
    changeRowsPerPage: ref.changeRowsPerPage,
    rowsPerPageOptions: ref.state.rowsPerPageOptions.map((r) => ({
      value: r,
      label: r,
    })),
    changePage: ref.changePage,
    totalRows: ref.state.displayData.length,
  };
};

const Pagination = forwardRef(({ tableRef }, ref) => {
  window.t = tableRef;
  const attrs = fetchTableAttributes(tableRef);
  console.log("attrs", attrs);

  const classes = useStyles();

  const [currentPage, setCurrentPage] = useState(attrs.currentPage);
  const [rowsPerPage, setRowsPerPage] = useState(attrs.rowsPerPage);
  const [totalPages, setTotalPages] = useState(attrs.totalPages);

  useImperativeHandle(ref, () => ({
    refreshPagination() {
      refresh();
    },
  }));

  const refresh = () => {
    const attrs = fetchTableAttributes(tableRef);
    console.log("userEffect attrs", attrs);
    setCurrentPage(attrs.currentPage);
    setRowsPerPage(attrs.rowsPerPage);
    setTotalPages(attrs.totalPages);
  };

  console.log(
    "tableRef.current.state.displayData.length",
    tableRef.current && tableRef.current.state.displayData.length
  );

  useEffect(() => {
    refresh();
  }, [attrs]);

  useEffect(() => {
    console.log("userEffect - total pages", totalPages);
  }, [totalPages]);

  const handleNextPage = () => {
    const nextPage = currentPage + 1;
    if (nextPage >= attrs.totalPages) return;
    setCurrentPage(nextPage);
    attrs.changePage(nextPage);
  };

  const handlePrevPage = () => {
    const prevPage = currentPage - 1;
    if (prevPage < 0) return;
    setCurrentPage(prevPage);
    attrs.changePage(prevPage);
  };

  const handleRowPerPageChange = (data) => {
    setRowsPerPage(data.value);
    attrs.changeRowsPerPage(data.value);
    setTotalPages(1 + Math.floor(attrs.totalRows / data.value));
  };

  console.log("totalPages", totalPages);

  return (
    <Box className={classes.root}>
      <Box className={classes.textLabel}>
        Showing <b>{currentPage + 1}</b> of {totalPages}:
      </Box>
      {/*<PageSelect
        options={attrs.rowsPerPageOptions}
        menuPlacement="top"
        value={
          attrs.rowsPerPageOptions &&
          attrs.rowsPerPageOptions.filter((r) => r.value === rowsPerPage)[0]
        }
        onChange={handleRowPerPageChange}
      />
      */}
      <FontAwesomeIcon
        className={classes.actionsButtons}
        disabled={currentPage === 0}
        onClick={handlePrevPage}
        icon={faArrowLeft}
      ></FontAwesomeIcon>
      <Box className={classes.paginationBlock}>{currentPage + 1}</Box>
      <FontAwesomeIcon
        className={classes.actionsButtons}
        disabled={currentPage >= totalPages - 1}
        onClick={handleNextPage}
        icon={faArrowRight}
      ></FontAwesomeIcon>
    </Box>
  );
});

export default Pagination;
